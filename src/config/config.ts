export namespace Config {

    /**
     * Verbose error log
     * @type {boolean}
     */
    export const VERBOSE: boolean = true;

    /**
     * @type {number}
     */
    export const MAX_HARVESTERS_PER_SOURCE: number = 1;

    /**
     * Default amout of minimal ticksToLive Screep can have before it goes to renew
     * @type {number}
     */
    export const DEFAULT_MIN_LIFE_BEFORE_NEEDS_REFILL: number = 700;
}