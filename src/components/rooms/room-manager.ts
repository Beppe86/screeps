import { Config } from "./../../config/config";
import { SourceManager } from "../sources/source-manager";
import { CreepPrefabs } from "../creeps/creep-prefabs";
import { Roles } from "../creeps/Roles";
import { SpawnManager } from "../spawns/spawn-manager";
// import {  } from "../../utils/PositionUtils";
import { QueueResolver } from "../creeps/queue-resolver";
import { spawn } from "child_process";
import { RoomWrapper } from "./room";
import { SourceMemory } from "../sources/source";

export namespace RoomManager {

    export var rooms: { [roomName: string]: RoomWrapper }
    export var roomNames: string[] = [];

    export var sources: { [room: string]: Source[] }
    export var spawnQueue: { [roomName: string]: CustomSpawnOptions[] }

    export function loadRooms() {
        rooms = _.mapValues(Game.rooms, room => new RoomWrapper(room));

        sources = {}

        _loadRoomNames();

        if (Config.VERBOSE) {
            let count = _.size(rooms);
            console.log(count + " room(s) found.");
        }
    }

    export function ValidateMemory(pos: RoomPosition) {

        _.forEach(rooms, function (room: RoomWrapper) {
            let sourceInfos = room._room.memory.sourceMemory;

            if (sourceInfos == null)
                sourceInfos = {}

            if (_.size(sourceInfos) == 0) {
                let sourceArray = SourceManager.getAllSources();
                let lowestDistanceInTiles = 999;
                // Sort sources based on proximity
                // let sortedSources = _.sortBy(sourceArray, source => pos.getRangeTo(source)).reverse();
                
                for (let i = 0; i < sourceArray.length; i++) {
                    let emptyNeigbhours = Game.getObjectById<Source>(sourceArray[i].id).pos.findEmptyNeighbours();
                    let distanceInTiles = pos.getRangeTo(sourceArray[i]);

                    if (distanceInTiles < lowestDistanceInTiles) {
                        lowestDistanceInTiles = distanceInTiles;
                    }

                    sourceInfos[sourceArray[i].id] = {
                        sourceId: sourceArray[i].id,
                        mainMinerWorkBodies: 0,
                        numberOfWorkerBodies: 0,
                        distanceInTick: 0,
                        distanceInTiles: pos.getRangeTo(sourceArray[i]),
                        isInitialSource: false, // First source is initialSource
                        harvesters: {},
                        miningPositions: emptyNeigbhours
                    } as SourceMemory
                }

                _.min(sourceInfos, source => source.distanceInTiles).isInitialSource = true;
            }

            room._room.memory.sourceMemory = sourceInfos;
        });
    }

    export function getFirstRoom(): RoomWrapper {
        return rooms[roomNames[0]];
    }

    export function iterateRoomStatus(): void {
        spawnQueue = {}

        _.forEach(rooms, function (room: RoomWrapper) {

            spawnQueue[room.name] = QueueResolver.ResolveSpawnQueue(room);
            if (Config.VERBOSE)
                console.log("SpawnQueue: " + spawnQueue[room.name].length);
        });


    }

    function _loadRoomNames() {
        for (let roomName in rooms) {
            if (rooms.hasOwnProperty(roomName)) {
                roomNames.push(roomName);
            }
        }
    }

    // Room.prototype.getSourceId = function (numberOfWork: number): string {
    //     let room: Room = this;
    //     let miningInfo = room.memory.sourceInfos;

    //     // Find the source with the least worker bodies stored in a SourceMininingInfo object
    //     let sourceInfo = _.min(miningInfo, function (info: SourceMiningInfo) { return info.numberOfWorkerBodies; });

    //     return sourceInfo.sourceId
    // }
}

Source.prototype.getWrapper = function () {
    let source = (this as Source);
    return RoomManager.rooms[source.room.name].source(source.id);
}


declare global {


    // interface SourceMiningSpot extends RoomPosition {
    //     minerId: string;
    // }

    interface Room {
        getSourceId(numberOfWork: number): string;
        markMiningPosition(sourceId: string, pos: RoomPosition, distanceInTick: number): void;
    }
}