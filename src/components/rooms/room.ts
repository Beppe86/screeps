import { SourceWrapper, SourceMemory } from "../sources/source";

export interface RoomWrapperInterface {
    readonly name: string
    readonly sources: SourceWrapper[];
}

declare global {
    interface RoomMemory {
        sourceMemory: { [sourceId: string]: SourceMemory }
        spawnQueue: CustomSpawnOptions[];
    }
}

export class RoomWrapper implements RoomWrapperInterface {
    _room: Room;

    constructor(room: Room) {
        this._room = room;
    }

    get name(): string {
        return this._room.name;
    }

    get sources(): SourceWrapper[] {
        return _.map(_.sortBy(this._room.memory.sourceMemory, mem => mem.distanceInTiles).reverse(), mem =>
            new SourceWrapper(mem, this));
    }


    get isAllSourcesSaturated(): boolean {
        var isAllSourcesSaturated = true;

        _.forEach(this.sources, source => {
            if (source.isSaturated == false) {
                isAllSourcesSaturated = false;
                return;
            }
        });

        return isAllSourcesSaturated;
    }

    source(sourceId: string): SourceWrapper {
        return new SourceWrapper(this._room.memory.sourceMemory[sourceId], this);
    }

    getSourceIdToHarvestAt(numberOfWork: number): string {
        return _.find(this.sources, source => source.availableWorkerBodies >= numberOfWork &&
            source.availableMiningSpotsLeft > 0).sourceId;
    }
}