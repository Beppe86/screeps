RoomPosition.prototype.compare = function (pos: RoomPosition): boolean {
    return (this.x === pos.x &&
        this.y === pos.y &&
        this.roomName === pos.roomName);
}

const adjacentX = [-1, 0, 1];
const adjacentY = [-1, 0, 1];

RoomPosition.prototype.findEmptyNeighbours = function (): RoomPosition[] {

    let adjacentPos: RoomPosition[] = [];

    for (let modifierX of adjacentX) {
        for (let modifierY of adjacentY) {
            if (modifierX === 0 && modifierY === 0) {
                continue;
            }
            let x = this.x + modifierX;
            let y = this.y + modifierY;

            let posLook = Game.map.getTerrainAt(x, y, this.roomName);
            if (posLook == "plain")
                adjacentPos.push(new RoomPosition(x, y, this.roomName));
        }
    }

    return adjacentPos;
}

// /**
//  * @param {number|object} x
//  * @param {number} [y]
//  * @param {string} [roomName]
//  * @returns {RoomPosition|boolean}
//  */
// var getRoomPosition = function (x, y, roomName) {
//     if (!x) return false;

//     if (typeof x == 'object') {
//         var object = x;

//         x = object.x;
//         y = object.y;
//         roomName = object.roomName || object.room;
//     }

//     return new RoomPosition(x, y, roomName);
// };

let getRoomPosition = function (pos: RoomPosition): RoomPosition {
    return new RoomPosition(pos.x, pos.y, pos.roomName);
}

export var pos = getRoomPosition;

declare global {
    interface RoomPosition {
        compare(pos: RoomPosition): boolean;
        findEmptyNeighbours(): RoomPosition[];
        // toRoomPosition(): RoomPosition;
    }
}