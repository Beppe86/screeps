import { Config } from "../../config/config";
import { RoomManager } from "../rooms/room-manager";
import { Roles } from "../creeps/Roles";
import { NameGenerator } from "../../utils/name-generator";
import { CreepWrapper } from "../creeps/roles/creep";


export namespace SpawnManager {
    export var spawns: { [spawnName: string]: StructureSpawn }
    export var spawnNames: string[] = [];
    export var spawnCount: number = 0;

    export function loadSpawns() {
        spawns = Game.spawns;
        spawnCount = _.size(spawns);

        _loadSpawnNames();

        if (Config.VERBOSE) {
            console.log(spawnCount + "spawns in room.");
        }
    }

    export function getFirstSpawn(): StructureSpawn {
        return spawns[spawnNames[0]];
    }

    function _loadSpawnNames() {
        for (let spawnName in spawns) {
            if (spawns.hasOwnProperty(spawnName)) {
                spawnNames.push(spawnName);
            }
        }
    }

    Spawn.prototype.createACreep = function (): void {
        /*
    This method will spawn pretty much anything you want. You have two options, exact or ratio
     
    Exact: An example of what's in the spawnQueue is my miner code, which in the beginning of a 
           new room builds up the body as the level allows:
    {
        exact: true,
        name: (unique name),
        body: {"work": 1, "carry": 1, "move": 1, "work": 4, "move": 2, "work": 1},
        memory: {role: 'miner', homeRoom: this.room.name, sourceId: opts.sourceId}
    }
     
    Ratio: This will take whatever ratio you give it and figure out the maximum amount your spawn                    
           can make.
           You can specify the maximum size of the creep or not.
           The body will create all the first body parts first, and then the next...like if you have 
           2 carrys and 1 moves in the ratio, it will create all the carrys first and then the moves 
     
        Example: Here is my refill cart spawnQueue code:
    {
        exact: false,
        maxBodyParts: 30,
        body: {"carry": 2, "move": 1},
        name: (unique name),
        memory: {role: 'refillCart', homeRoom: this.room.name, working: false}
    }   
    */
        let spawn: StructureSpawn = this;
        if (spawn.spawning || RoomManager.spawnQueue[spawn.room.name].length === 0) return;
        let opts: CustomSpawnOptions = RoomManager.spawnQueue[spawn.room.name][0];
        let body: BodyPartConstant[] = []; let spawnResult: ScreepsReturnCode;

        let maxBodyParts = 50;

        // Pull maximum possible energy to be spent
        let maxEnergy = this.room.energyCapacityAvailable;

        if (opts.exact) {
            // cycle through the body parts in options
            _.forEach(opts.body, function (bodyPart) {
                if (BODYPART_COST[bodyPart.type] > maxEnergy || maxBodyParts === 0) return false;

                for (let i = 0; i < bodyPart.number; i++) {
                    // check next body part costs and 50 body part limit
                    if (BODYPART_COST[bodyPart.type] > maxEnergy || maxBodyParts === 0) {
                        maxEnergy = 0; break;
                    }

                    // push body part into body array
                    body.push(bodyPart.type as BodyPartConstant);

                    // decrease maximum energy allowed for the next iteration
                    maxEnergy -= BODYPART_COST[bodyPart.type];

                    // decrement the 50 body part limit
                    maxBodyParts--;
                }

                return true;
            });
        }
        else { }

        let spawnOptions: SpawnOptions = {
            memory: opts.memory
        }

        let name = NameGenerator.getRandomName(opts.memory.role);

        // attempt to spawn creep with passed memory, name and formed creep body
        spawnResult = spawn.spawnCreep(body, name, spawnOptions);
        
        // Pull creep out of spawn order if success
        if (spawnResult == OK) {
            _.pullAt(RoomManager.spawnQueue[spawn.room.name], [0]);
            opts.callback(name, spawnOptions.memory as HarvesterMemory, body);
        }
    }
}