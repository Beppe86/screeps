import { Config } from "./../../config/config";


export namespace ConstructionManager {
    export var sites: { [siteName: string]: ConstructionSite };
    export var siteNames: string[] = [];

    export var constructionFlags: { [name: string]: Flag };
    export var flagNames: string[] = [];

    export var siteCount: number = 0;

    export function loadSites(): void {
        sites = Game.constructionSites;
        constructionFlags = {};
        _loadConstructionFlags();

        siteCount = _.size(sites) + _.size(constructionFlags)

        _loadSiteNames();
        _loadConstructionFlagNames();

        if (Config.VERBOSE) {
            console.log(siteCount + " construction sites found");
        }
    }

    function _loadConstructionFlags(): void {
        _.forEach(Game.flags, function (flag: Flag) {
            if (flag.color === COLOR_YELLOW) {
                constructionFlags[flag.name] = flag;
            }
        });
    }

    function _loadSiteNames(): void {
        for (let siteName in sites) {
            if (sites.hasOwnProperty(siteName)) {
                siteNames.push(siteName);
            }
        }
    }

    function _loadConstructionFlagNames(): void {
        for (let flagName in constructionFlags) {
            if (constructionFlags.hasOwnProperty(flagName)) {
                flagNames.push(flagName);
            }
        }
    }
}