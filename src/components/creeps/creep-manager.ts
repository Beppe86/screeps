import { Config } from "./../../config/config";
import { SourceManager } from "./../sources/source-manager";
import { SpawnManager } from "../spawns/spawn-manager";
import { BaseHarvester } from "./roles/Harvester/base-harvester";
import { Roles } from "./Roles";
import { Initializer } from "./roles/Initializer";
import { RoomManager } from "../rooms/room-manager";
import { pos } from "../rooms/room-positions";
import { SourceWrapper } from "../sources/source";

// import { } from "../../types";

export namespace CreepManager {

    export var creeps: { [creepName: string]: Creep };
    export var creepNames: string[] = [];
    export var creepCount: number = 0;

    export function loadCreeps(): void {
        creeps = Game.creeps;
        creepCount = _.size(creeps);

        _loadCreepNames();

        if (Config.VERBOSE) {
            console.log(creepCount + " creep(s) found");
        }
    }

    export function isHarvesterLimitFull(): boolean {
        // TODO: Look into some load balancer here, instead of loading all at start
        return (Config.MAX_HARVESTERS_PER_SOURCE == creepCount);
    }

    export function ResolveCreepActions(): void {
        _.forEach(creeps, function (creep: Creep) {
            if (creep.spawning) return true;

            if (creep.memory.role === Roles.Initializer) {
                let initializer = new Initializer(creep);
                initializer.action();
            }

            return true;
        });
    }
    export function harvesterGoToWork(): void {

        let harvesters: BaseHarvester[] = [];
        _.forEach(creeps, function (creep: Creep) {
            if (creep.memory.role == Roles.Harvester) {
                let harvester = new BaseHarvester(creep);

                // Next move for harvester
                harvester.action();

                // Save harvester to collection
                harvesters.push(harvester);
            }
        });

        if (Config.VERBOSE) {
            console.log(harvesters.length + " harvesters reported on duty today!");
        }
    }

    export function deleteCreep(name: string, memory: CreepMemory): void {
        console.log("delete creep" + name);

        if (memory.role == Roles.Harvester || memory.role == Roles.Initializer) {
            let harvesterMemory = memory as HarvesterMemory;
            let miningPos = harvesterMemory.target_mining_pos;

            let room = RoomManager.rooms[harvesterMemory.room];
            let source: SourceWrapper = null;
            if (harvesterMemory.target_source_id != null)
                source = room.source(harvesterMemory.target_source_id);
            else {
                // if target_source_id is null we need to find source by target_mining_pos
                source = room.source(pos(miningPos).findClosestByRange(FIND_SOURCES).id);
            }

            source.deRegisterHarvester(name, miningPos);

            if (Config.VERBOSE) {
                console.log("Successfully reapplied mining position");
            }
        }
    }

    function _loadCreepNames(): void {
        for (let creepName in creeps) {
            if (creeps.hasOwnProperty(creepName)) {
                creepNames.push(creepName);
            }
        }
    }
}