import { Roles } from "./Roles";
import { CreepPrefabs } from "./creep-prefabs";
import { SourceManager } from "../sources/source-manager";
import { RoomWrapper } from "../rooms/room";
// import { PositionUtils } from "../../utils/PositionUtils";

export namespace QueueResolver {

    export function ResolveSpawnQueue(room: RoomWrapper): CustomSpawnOptions[] {
        let spawnQueue: CustomSpawnOptions[] = [];

        _.forEach(room.sources, function (source) {

            // Check if initialization is finished

            if (source.numberOfWorkerBodies < 6) {

                let creep: CustomSpawnOptions = null;
                let memory: HarvesterMemory = {
                    role: Roles.Initializer,
                    room: room.name,
                    target_source_id: source.sourceId
                } as HarvesterMemory

                // first check main miner
                if (source.mainMinerWorkerBodies == 0 &&
                    source.hasContainerStructure) {
                    console.log("Main miner needed")
                    creep = CreepPrefabs.MinerCreep

                    // memory.target_mining_pos = source.positionOfContainer;
                    // creep.callback = CreepPrefabs.harvesterCallback(source)
                }
                // else check for supplementary miners
                else if (source.availableWorkerBodies > 0) {
                    creep = CreepPrefabs.BasicCreep;
                    let miningPositions = source.availableMiningSpotsLeft;

                    // No need creating a miner if there are no available positions left
                    if (miningPositions == 0) return true;

                    // This is for an edge case where one too many harvesters might get spawned before 
                    if (source.positionOfContainer == null && // container position is created
                        source.numberOfWorkerBodies > 0 && // By checking if any creep is already created for source
                        _.size(source.miningPositions) == 1) { // and only one mining position left
                            return true; // We wait until positionOfContainer is established before spawning more creeps
                        }

                    // Add another work if possible
                    // if (source.availableWorkerBodies > 1) {
                    //     creep.body.push({ type: WORK, number: 1 });
                    // }

                    creep.callback = CreepPrefabs.harvesterCallback(source);
                }
                if (creep != null) {
                    creep.memory = memory;
                    spawnQueue.push(creep);
                }
            }
            return true;
        });

        return spawnQueue;
    }
}