import { CreepWrapper } from "./roles/creep";
import { SourceWrapper } from "../sources/source";
import { BaseHarvester } from "./roles/Harvester/base-harvester";

export namespace CreepPrefabs {
    const BasicBody: { type: BodyPartConstant, number: number }[] = [
        { type: WORK, number: 1 },
        { type: CARRY, number: 1 },
        { type: MOVE, number: 1 }
    ];

    export const BasicCreep: CustomSpawnOptions = {
        exact: true,
        body: BasicBody,
        memory: null
    }

    export const MinerCreep: CustomSpawnOptions = {
        exact: true,
        body: BasicBody.concat({ type: WORK, number: 5 }),
        memory: null
    }

    export function harvesterCallback(source: SourceWrapper): (name: string, memory: HarvesterMemory, body: BodyPartConstant[]) => void {
        return function (name: string, memory: HarvesterMemory, body: BodyPartConstant[]) {
            
            let numberOfWork = _.filter(body, part => part == WORK).length;
            
            memory.target_mining_pos = source.registerHarvester(name, numberOfWork);
            memory.target_source_id = source.sourceId;
        };
    }

    // export function secondaryMinerCallback(sourceInfo: SourceMiningInfo): (body: BodyPartConstant[]) => void {
    //     return function (body: BodyPartConstant[]) {
    //         var work = body.filter(function (part) { return part == WORK; }).length;
    //         // sourceInfo.numberOfWorkerBodies += work;

    //         // pulls mining position since it is now used
    //         // _.pullAt(sourceInfo.miningPositions, [0]);
    //     }
    // }
}