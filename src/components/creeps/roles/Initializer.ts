import { CreepWrapper } from "./creep";
import { BaseHarvester } from "./Harvester/base-harvester";
import { Builder } from "./builder";
import { InitialHarvester } from "./Harvester/initial-harvester";


export class Initializer extends CreepWrapper {

    constructor(creep: Creep) {
        super(creep);
    }
    public action(): boolean {
        if (this.isBagEmpty() || this.isHarvester()) {
            this.beHarvester();
        } else if (this.isBagFull() || this.isBuilder()) {
            this.beBuilder();
        }
        return true;
    }

    private beBuilder(): void {
        (this.creep.memory as HarvesterMemory).target_source_id = null;
        console.log(this.creep.name + ": Im a builder!");
        let builder = new Builder(this.creep);
        builder.action();
    }

    private beHarvester(): void {
        (this.creep.memory as BuilderMemory).target_building_site_id = null;

        let harvester = new InitialHarvester(this.creep);
        harvester.action();
    }

    private isBuilder(): boolean {
        return ((this.creep.memory as BuilderMemory).target_building_site_id != null);

    }

    private isHarvester(): boolean {
        return ((this.creep.memory as HarvesterMemory).target_source_id != null);

    }
}