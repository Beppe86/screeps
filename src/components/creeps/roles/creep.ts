import { Config } from "../../../config/config";


export interface CreepWrapperInterface {
    creep: Creep;
    renewStation: StructureSpawn;
    _minLifeBeforeNeedsRenew: number;

    moveTo(target: RoomPosition | { pos: RoomPosition }): number;
    isBagFull(): boolean;
    needsRenew(): boolean;
    tryRenew(): number;
    moveToRenew(): void;

    action(): boolean;
}

export class CreepWrapper implements CreepWrapperInterface {

    public creep: Creep;
    public renewStation: StructureSpawn;

    public _minLifeBeforeNeedsRenew: number = Config.DEFAULT_MIN_LIFE_BEFORE_NEEDS_REFILL;

    constructor(creep : Creep) {
        this.creep = creep;
        this.renewStation = <StructureSpawn>Game.getObjectById(this.creep.memory.renew_station_id);
    }

    public isBagEmpty(): boolean {
        return (this.creep.carry.energy === 0);
    }

    public isBagFull(): boolean {
        return (this.creep.carry.energy == this.creep.carryCapacity);
    }

    moveTo(target: RoomPosition | { pos: RoomPosition; }) {
        return this.creep.moveTo(target);
    }

    needsRenew(): boolean {
        return (this.creep.ticksToLive < this._minLifeBeforeNeedsRenew);
    }


    public tryRenew(): ScreepsReturnCode {
        return this.renewStation.renewCreep(this.creep);
    }

    public moveToRenew(): void {
        if (this.tryRenew() == ERR_NOT_IN_RANGE) {
            this.moveTo(this.renewStation);
        }
    }

    public action(): boolean {
        return true;
    }

    public countBodyParts(bodyPart: BodyPartConstant): number {
        return _.filter(this.creep.body, body =>  body.type == bodyPart).length;
    } 
}