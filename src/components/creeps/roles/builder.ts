import { CreepWrapper } from "./creep";
import { SourceManager } from "../../sources/source-manager";
import { RoomManager } from "../../rooms/room-manager";
import { ConstructionManager } from "../../constructions/construction-manager";


export class Builder extends CreepWrapper {

    public targetBuildingSite: ConstructionSite;

    constructor(creep: Creep) {
        super(creep);

        let memory = creep.memory as BuilderMemory;
        let siteId = memory.target_building_site_id;

        this.targetBuildingSite = Game.getObjectById(siteId);
    }

    public tryBuild(): ScreepsReturnCode {
        return this.creep.build(this.targetBuildingSite);
    }

    public moveToBuild(): void {
        if (this.tryBuild() == ERR_NOT_IN_RANGE) {
            this.moveTo(this.targetBuildingSite);
        }
    }

    public action(): boolean {

        if (this.targetBuildingSite == null) {
            let availableSites = _.filter(Game.constructionSites, site => site.room == this.creep.room);
            let orderedSites = _.sortBy(availableSites, site => site.pos.getRangeTo(this.creep.pos));

            if (_.size(orderedSites) > 0) {
                this.targetBuildingSite = orderedSites[0];
                (this.creep.memory as BuilderMemory).target_building_site_id = orderedSites[0].id;
            }
            else {
                // nothing to do as builder, TODO
            }
        }
        else {
            this.moveToBuild();
        }

        return true;
    }
}

declare global {
    interface BuilderMemory extends CreepMemory {
        target_building_site_id: string;
    }
}