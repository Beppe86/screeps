import { BaseHarvester, HarvesterInterface } from "./base-harvester";
import { SourceManager } from "../../../sources/source-manager";
import { CreepWrapper } from "../creep";
import { RoomManager } from "../../../rooms/room-manager";

export class InitialHarvester extends BaseHarvester {
    constructor(creep: Creep) {
        super(creep);

        if (this.isBagFull() && this.targetSource.getWrapper().shouldEnergyBeReturned == false) {
            this.resetMiningPosition();
        }

        // let memory = this.getMemory();
        // if (this.targetSource == null) {
        //     console.log("missing source");
        //     this.targetSource = this.getMiningSource();
        //     this.setMiningSource(this.targetSource);
        // }
    }

    private resetMiningPosition(): void {
        this._memory.target_source_id = null;
    }

    // private getMiningSource(): Source {
    //     var sources = SourceManager.getAllSources();
    //     var sourcesSortedByDistance = _.sortBy(sources, s => this.creep.pos.getRangeTo(s));
    //     return <Source>Game.getObjectById(sourcesSortedByDistance[0].id);
    // }

    // private setMiningSource(source: Source): void {
    //     console.log("register");
    //     this.targetSource = source;
    //     this.getMemory().target_source_id = this.targetSource.id;

    //     source.registerMiner(this);
    // }
}