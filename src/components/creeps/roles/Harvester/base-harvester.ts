import { CreepWrapper, CreepWrapperInterface } from "../creep";
import { SpawnManager } from "../../../spawns/spawn-manager";
import { RoomManager } from "../../../rooms/room-manager";
import "../../../rooms/room-positions";
import { pos } from "../../../rooms/room-positions";
import { SourceWrapper } from "../../../sources/source";
import { Config } from "../../../../config/config";

// import { PositionUtils } from "../../../utils/PositionUtils";

// import { } from "../../../types";

export interface HarvesterInterface {

    // targetSource: Source;
    // targetEnergyDropOff: StructureSpawn | Structure;

    tryHarvest(markPosition: (pos: RoomPosition) => void): ScreepsReturnCode;
    moveToHarvest(): void;
    tryEnergyDropOff(): number;
    moveToDropEnergy(): void;

    action(): boolean;
}

export class BaseHarvester extends CreepWrapper implements HarvesterInterface, CreepWrapperInterface {
    protected _memory: HarvesterMemory;

    protected targetSource: Source;
    private target_mining_pos: RoomPosition;
    private targetEnergyDropOff: StructureSpawn | Structure;
    // public markPosition: (pos: RoomPosition) => void

    constructor(creep: Creep) {
        super(creep);

        this._memory = this.creep.memory as HarvesterMemory;

        if (this._memory.target_mining_pos != null) {
            this.target_mining_pos = this._memory.target_mining_pos;
        }

        if (this._memory.target_source_id == null) {
            this._memory.target_source_id = RoomManager.rooms[creep.pos.roomName].getSourceIdToHarvestAt(this.countBodyParts(WORK));
            RoomManager.rooms[creep.pos.roomName].source(this._memory.target_source_id).registerHarvester(creep.name, this.countBodyParts(WORK));
        }

        this.targetSource = <Source>Game.getObjectById(this._memory.target_source_id);

        let energyDropOfId = this._memory.target_energy_dropoff_id;

        if (energyDropOfId == null) {
            energyDropOfId = SpawnManager.getFirstSpawn().id;
        }

        this.targetEnergyDropOff = <StructureSpawn | Structure>Game.getObjectById(SpawnManager.getFirstSpawn().id);
    }

    public action(): boolean {
        if (this.isBagFull()) {
            console.log("bag is full")
            this.moveToDropEnergy();
        } else {
            if (this.target_mining_pos == null) {
                this.findMiningPosition();
            }
            this.moveToHarvest();
        }

        return true;
    }

    public tryHarvest(): ScreepsReturnCode {
        return this.creep.harvest(this.targetSource);
    }

    public moveToHarvest(): void {
        if (this.target_mining_pos != null && this.creep.pos.compare(this.target_mining_pos)) {
            this.tryHarvest();
        }
        else {
            let harvestPos: RoomPosition = (this.target_mining_pos == null) ? this.targetSource.pos : this.target_mining_pos;
            this.moveTo(pos(harvestPos));
        }
    }

    public tryEnergyDropOff(): ScreepsReturnCode {
        return this.creep.transfer(this.targetEnergyDropOff, RESOURCE_ENERGY);
    }

    public moveToDropEnergy(): void {
        if (this.tryEnergyDropOff() == ERR_NOT_IN_RANGE) {
            this.moveTo(this.targetEnergyDropOff);
        }
    }

    private findMiningPosition(): void {
        if (Config.VERBOSE) {
            console.log("Looking for best mining spot");
        }

        // Marks start tick when harvester starts moving towards source
        if (this.getFindSourceStartTick == null) {
            this.setFindSourceStartTick = Game.time;
        }

        if (OK === this.tryHarvest()) {
            this.markPosition();
        } else {
            this.moveToHarvest();
        }
    }

    private markPosition(): void {
        let timeToReachSource = Game.time - this.getFindSourceStartTick;
        this.targetSource.getWrapper().markMiningPosition(this.creep.pos, this.countBodyParts(WORK), timeToReachSource);

        // update creep memory accordingly
        this._memory.target_mining_pos = this.creep.pos;
        this.setFindSourceStartTick = null;
    }

    private get getFindSourceStartTick(): number {
        return this._memory.find_source_start_tick;
    }

    private set setFindSourceStartTick(tick: number) {
        this._memory.find_source_start_tick = tick;
    }
}

declare global {
    interface HarvesterMemory extends CreepMemory {
        target_energy_dropoff_id?: string;
        target_source_id: string;
        target_mining_pos?: RoomPosition;
        find_source_start_tick?: number;
    }
}