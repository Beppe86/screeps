import { Config } from "./../../config/config";

export namespace SourceManager {
    export var sources: Source[];
    export var sourceCount: number = 0;

    export function loadSources(room: Room) {
        sources = room.find(FIND_SOURCES_ACTIVE);
        sourceCount = _.size(sources);

        if (Config.VERBOSE) {
            console.log(sourceCount + " sources in room.");
        }
    }

    export function getFirstSource(): Source {
        return sources[0];
    }

    export function getAllSources(): Source[] {
        return sources;
    }


}