import { BaseHarvester } from "../creeps/roles/Harvester/base-harvester";
import { RoomManager } from "../rooms/room-manager";
import { RoomWrapper } from "../rooms/room";

export interface SourceMemory {
    sourceId: string;
    numberOfWorkerBodies: number;
    mainMinerWorkBodies: number;
    miningPositions: RoomPosition[];
    positionOfContainer: RoomPosition;
    distanceInTick: number;
    distanceInTiles: number;
    isInitialSource: boolean;

    harvesters: { [name: string]: number };
}

export interface SourceWrapperInterface {
    registerHarvester(name: string, numberOfWork: number): void;
    // deRegisterHarvester(name: string, numberOfWork: number): void;
}

export class SourceWrapper implements SourceWrapperInterface {
    private _memory: SourceMemory;
    private _roomWrapper: RoomWrapper;

    constructor(memory: SourceMemory, room: RoomWrapper) {
        this._memory = memory;
        this._roomWrapper = room;
    }

    private get harvesters(): { [name: string]: number } {
        return this._memory.harvesters;
    }

    get sourceId(): string {
        return this._memory.sourceId;
    }

    get numberOfWorkerBodies(): number {
        return this._memory.numberOfWorkerBodies;
    }

    set numberOfWorkerBodies(number: number) {
        this._memory.numberOfWorkerBodies = number;
    }

    get availableWorkerBodies(): number {
        return 6 - this._memory.numberOfWorkerBodies;
    }

    get numberOfMainWorkerBodies(): number {
        return this._memory.mainMinerWorkBodies;
    }

    get hasContainerStructure(): boolean {
        return (this._memory.positionOfContainer != null &&
            this._memory.positionOfContainer.lookFor(LOOK_STRUCTURES) != null);
    }

    set distanceInTick(ticks: number) {
        this._memory.distanceInTick = ticks;
    }

    set positionOfContainer(pos: RoomPosition) {
        this._memory.positionOfContainer = pos;
    }

    get positionOfContainer(): RoomPosition {
        return this._memory.positionOfContainer;
    }

    set mainMinerWorkerBodies(workerBodies: number) {
        this._memory.mainMinerWorkBodies = workerBodies;
    }

    /**
     * Source decides if it should keep it`s energy to be used for it`s container or if it should be returned
     * for use to the rest of the room
     */
    get shouldEnergyBeReturned(): boolean {
        if (this.positionOfContainer == null) {
            console.log("Missing positionContainer");
            return true; // We can`t spend energy on container if no one is marked for some reason
        }

        if (this._memory.isInitialSource == false) {
            console.log("Missing isInitialSource");

            return false; // Only initial source considers returning energy instead of constructing container
        }

        // this method takes the most CPU is it is at the bottom
        // InitialSource only constructs it`s container after all other sources have been saturated with workers
        return this._roomWrapper.isAllSourcesSaturated == false;
    }

    get isSaturated(): boolean {
        return (this.availableWorkerBodies <= 0) || (this.availableMiningSpotsLeft <= 0);
    }

    get availableMiningSpotsLeft(): number {
        return this._memory.miningPositions.length;
    }

    get miningPositions(): RoomPosition[] {
        return this._memory.miningPositions;
    }

    registerHarvester(name: string, numberOfWork: number): RoomPosition {
        this.harvesters[name] = numberOfWork;
        this.numberOfWorkerBodies += numberOfWork;

        // We let pathfinding find positionOfContainer which uses markMiningPosition to claim miningPosition
        // When positionOfContainer is set we randomly assign miningPositions to prevent two creeps figting over one spot
        if (this.positionOfContainer != null)
            return this.miningPositions.pop();

        return null;
    }

    public deRegisterHarvester(name: string, miningPos: RoomPosition): void {
        this.numberOfWorkerBodies -= this.harvesters[name];

        if (miningPos != null) {
            this.miningPositions.push(miningPos);

        }
        delete this.harvesters[name];
    }

    markMiningPosition(miningPos: RoomPosition, workerBodies: number, distanceInTick: number): void {

        if (this.positionOfContainer == null) {
            if (OK !== this._roomWrapper._room.createConstructionSite(miningPos, STRUCTURE_CONTAINER)) {
                console.log("ERROR PLACING CONTAINER AT MINING POSITION");
            }

            this.distanceInTick = distanceInTick;
            this.positionOfContainer = miningPos;
        }

        _.remove(this.miningPositions, pos => miningPos.compare(pos));
    }
}

declare global {
    interface Source {
        getWrapper(): SourceWrapper;
    }
}


