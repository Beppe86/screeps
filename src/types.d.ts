// /// <reference path= "../node_modules/@types/screeps/index.d.ts" /> 

import { Roles } from "./components/creeps/roles";
import { BaseHarvester } from "./components/creeps/roles/Harvester/base-harvester";
import { CreepWrapper } from "./components/creeps/roles/creep";
// import "./components/rooms/room-positions";

// example declaration file - remove these and add your own custom typings

declare global {

  // memory extension samples
  export class CreepMemory {
    role: Roles;
    room: string;
    // working: boolean;beppe
    // harvestPointId: string;
    renew_station_id: string;
  }

  interface CustomSpawnOptions extends SpawnOptions {
    exact: boolean;
    maxBodyParts?: number;
    body: { type: BodyPartConstant, number: number }[];
    memory: CreepMemory;
    callback?(name: string, creepMemory: HarvesterMemory, body: BodyPartConstant[]): void;
  }

  interface StructureSpawn {
    createACreep(): void;
  }
  // interface Creep {
  //   CountBodyparts(body: BodyPartConstant): number;
  // }

  interface Memory {
    uuid: number;
    log: any;
  }
}
// `global` extension samples
declare namespace NodeJS {
  interface Global {
    log: any;
  }
}
