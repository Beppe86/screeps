import { CreepManager } from "./components/creeps/creep-manager";
import { RoomManager } from "./components/rooms/room-manager";
import { SpawnManager } from "./components/spawns/spawn-manager";
import { SourceManager } from "./components/sources/source-manager";
import { ConstructionManager } from "./components/constructions/construction-manager";
// import { PositionUtils } from "./utils/PositionUtils";

// Import my custom types, is there a better way?
import { } from "./types";

export namespace GameManager {
    export function globalBootstrap() {
        // Set up your global objects.
        // This method is executed only when Screeps system instantiated new "global"

        console.log("RUNNING GLOBAL SECTION");
        RoomManager.loadRooms();
        SourceManager.loadSources(RoomManager.getFirstRoom()._room);
        SpawnManager.loadSpawns();

        RoomManager.ValidateMemory(SpawnManager.getFirstSpawn().pos);

    }
    
    export function loop() {
        // Loop code start here
        // This is executed every tick

        CreepManager.loadCreeps();
        ConstructionManager.loadSites();

        CreepManager.ResolveCreepActions();
        RoomManager.iterateRoomStatus();
        SpawnManager.getFirstSpawn().createACreep();
    }
}