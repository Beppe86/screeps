import { ErrorMapper } from "./../utils/ErrorMapper";
import { GameManager } from './../game-manager';
import { CreepManager } from "../components/creeps/creep-manager";

declare var module: any;

// Singleton object. Since GameManager doesn't need  
GameManager.globalBootstrap();


// When compiling TS to JS and bundling with rollup, the line numbers and file names in error messages change
// This utility uses source maps to get the line numbers and file names of the original, TS source code
export const loop = ErrorMapper.wrapLoop(() => {

  var timeAsString = Game.time.toString();
  var twoLastDigit = timeAsString.slice(-2);
  if (+twoLastDigit % 60 == 0)
    console.log(`Current game tick is ${Game.time}`);

  GameManager.loop();


  // Automatically delete memory of missing creeps
  for (const name in Memory.creeps) {
    if (name in Game.creeps == false) {
      CreepManager.deleteCreep(name, Memory.creeps[name]);
      delete Memory.creeps[name];
    }
  }

  // Automatically delete memory of missing rooms
  for (const name in Memory.rooms) {
    if (!(name in Game.rooms)) {
      delete Memory.rooms[name];
    }
  }
});
